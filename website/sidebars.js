module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Introduction',
      items: [
          'introduction',
      ],
    },
    {
      type: 'category',
      label: 'Fondamentaux',
      items: [
        'architecture',
        'technologies',
      ],
    },
    {
      type: 'category',
      label: 'Technique',
      items: [
        'code-structure',
        'dev-local',
        'deploiement'
      ],
    },
    {
      type: 'category',
      label: 'Documentation utilisateur',
      items: [
        'fonctionnalites',
      ],
    },
  ],
};
