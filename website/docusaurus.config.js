/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Covid Alert Documentation',
  tagline: 'Comprendre le projet',
  url: 'https://CovidAlert.gitlab.io',
  baseUrl: '/covid-alert-documentation/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'covid-alert-documentation', // Usually your repo name.
  themeConfig: {
    prism: {
      additionalLanguages: ['powershell','docker','java'],
    },
    navbar: {
      title: 'Covid Alert',
      logo: {
        alt: 'My Site Logo',
        src: 'img/anti-virus.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Documentation',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/c3092',
          label: 'Projet GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Le projet',
          items: [
            {
              label: 'URL de deploiement',
              href: 'https://covid-alert-polytech.cluster-ig5.igpolytech.fr/',
            },
          ],
        },
        {
          title: 'Les sources',
          items: [
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/c3092',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Covid Alert, Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
